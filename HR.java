package com.company;

public class HR extends Department
{
    private Employee headOfHR;
    String nameOfDep;

    private Employee[] empHR;

    public HR()
    {
        int i=0;
        empHR= new Employee[i];
        populateHR();
        setHeadOfHR(headOfHR);
        getHeadOfHR();
        getNameOfDep();
        numOfEmpHR();
    }

    public void populateHR()
    {
        int k=0;
        while(k>0)
        {
            empHR[k]= new Employee();
        }
        setHeadOfHR(headOfHR);
    }

    private void setHeadOfHR(Employee headOfHR)
    {
        this.headOfHR = headOfHR;
    }

    public Employee getHeadOfHR()
    {
        return headOfHR;
    }

    public String getNameOfDep() {
        return nameOfDep;
    }

    public int numOfEmpHR()
    {
        return empHR.length;
    }
}
