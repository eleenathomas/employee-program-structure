package com.company;

public class PROD extends Department
{
    private Employee headOfPROD;
    String nameOfDep;

    private Employee[] empPROD;

    public PROD()
    {
        int i=0;
        empPROD= new Employee[i];
        populatePROD();
        setHeadOfPROD(headOfPROD);
        getHeadOfPROD();
        getNameOfDep();
        numOfEmpPROD();
    }

    public void populatePROD()
    {
        int k=0;
        while(k>0)
        {
           empPROD[k]= new Employee();
        }
        setHeadOfPROD(headOfPROD);
    }

    private void setHeadOfPROD(Employee headOfPROD)
    {
        this.headOfPROD = headOfPROD;
    }

    public Employee getHeadOfPROD()
    {
        return headOfPROD;
    }

    public String getNameOfDep()
    {
        return nameOfDep;
    }

    public int numOfEmpPROD()
    {
        return empPROD.length;
    }
}
