package com.company;

public class OP extends Department
{
    private Employee headOfOP;
    String nameOfDep;

    private Employee[] empOP;

    public OP()
    {
        int i=0;
        empOP= new Employee[i];
        populateOP();
    }

    public void populateOP()
    {
        int k=0;
        while(k>0)
        {
            empOP[k]= new Employee();
        }
        setHeadOfOP(headOfOP);
        getHeadOfOP();
        getNameOfDep();
        numOfEmpOP();
    }

    private void setHeadOfOP(Employee headOfOP)
    {
        this.headOfOP = headOfOP;
    }

    public Employee getHeadOfOP()
    {
        return headOfOP;
    }

    public String getNameOfDep()
    {
        return nameOfDep;
    }

    public int numOfEmpOP()
    {
        return empOP.length;
    }
}
