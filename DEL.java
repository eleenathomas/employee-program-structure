package com.company;

class DEL extends Department
{
    private Employee headOfDEL;
    String nameOfDep;

    private Employee[] empDEL;

    public DEL()
    {
        int i=0;
        empDEL= new Employee[i];
        populateDEL();
        getNameOfDep();
        setHeadOfDEL(headOfDEL);
        getHeadOfDEL();
        numOfEmpDEL();
    }

    public void populateDEL()
    {
        int k=0;
        while(k>0)
        {
            empDEL[k]= new Employee();
        }
    }

    private void setHeadOfDEL(Employee headOfDEL)
    {
        this.headOfDEL = headOfDEL;
    }

    public Employee getHeadOfDEL()
    {
        return headOfDEL;
    }

    public String getNameOfDep()
    {
        return nameOfDep;
    }

    public int numOfEmpDEL()
    {
        return empDEL.length;
    }

}
