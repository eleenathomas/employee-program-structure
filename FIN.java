package com.company;

public class FIN extends Department
{
    private Employee headOfFIN;
    String nameOfDep;

    private Employee[] empFIN;

    public FIN()
    {
        int i=0;
        empFIN= new Employee[i];
        populateFIN();
    }

    public void populateFIN()
    {
        int k=0;
        while(k>0)
        {
            empFIN[k]= new Employee();
        }
        setHeadOfFIN(headOfFIN);
        getHeadOfFIN();
        getNameOfDep();
        numOfEmpFIN();
    }

    private void setHeadOfFIN(Employee headOfFIN)
    {
        this.headOfFIN = headOfFIN;
    }

    public Employee getHeadOfFIN()
    {
        return headOfFIN;
    }

    public String getNameOfDep() {
        return nameOfDep;
    }

    public int numOfEmpFIN()
    {
        return empFIN.length;
    }
}
